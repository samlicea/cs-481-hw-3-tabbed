﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TabbedViewXamarin
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : TabbedPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        // Sends user to page 1 (Level 1)
        async private void Level1(Object Sender, EventArgs e)
        {
            
           await Navigation.PushAsync(new Page1());
        }

        // Sends user to level 2
        async private void Level2(Object Sender, EventArgs e)
        {

            await Navigation.PushAsync(new Page2());
        }

        // Sends user to Level 3
        async private void Level3(Object Sender, EventArgs e)
        {

            await Navigation.PushAsync(new Page3());
        }
    }



}

 