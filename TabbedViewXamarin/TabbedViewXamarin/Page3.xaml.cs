﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TabbedViewXamarin.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TabbedViewXamarin
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page3 : ContentPage
    {
        public Page3()
        {
            InitializeComponent();
        }

        // On appear event 
        protected override async void OnAppearing()
        {
            base.OnAppearing();

            var answer = await DisplayAlert("Question?", "Are you ready for Level 3?", "Yes", "No");

            // User selected yes, continues to page
            if (answer == true)
            {
                return;
            }

            else // User selected no - Goes back to home
            {
                await Navigation.PopToRootAsync();
            }

            // await DisplayAlert("Alert!", "You've been warned", "OK");

        }

        // User wins the game, moves on 
        async private void ImageButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Win());
        }

        // User loses the game, gets option to go back
        async private void ImageButton_Clicked_1(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Fail());
        }
    }
}