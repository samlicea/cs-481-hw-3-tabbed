﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TabbedViewXamarin.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Fail : ContentPage
    {
        public Fail()
        {
            InitializeComponent();
        }

        // On appear event 
        protected override async void OnAppearing()
        {
            base.OnAppearing();

            await DisplayAlert("Alert!", "You Chose the Wrong Answer", "Whoops");

        }

        // Takes User to previous page
        async private void PrevPage(object sender, EventArgs e)
        {
           await Navigation.PopAsync();
        }

        // Goes back home - Quit game
        async private void Button_Clicked(object sender, EventArgs e)
        {
           await Navigation.PopToRootAsync();
        }
    }
}