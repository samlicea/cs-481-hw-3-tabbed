﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TabbedViewXamarin.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Win : ContentPage
    {
        public Win()
        {
            InitializeComponent();
        }

        // On appear event 
        protected override async void OnAppearing()
        {
            base.OnAppearing();

            await DisplayAlert("Alert!", "Congratulations", "Ok");

        }

        // Goes back to home page
        async private void GoHome(object sender, EventArgs e)
        {
           await Navigation.PopToRootAsync();
        }

    }
}